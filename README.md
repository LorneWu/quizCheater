
# 環境   
windows10   
python 3.6 64bit   
adb (not yet)   
charles   


# problem   
1. 無法知道頁面切換時間點 > 無法自動按按鈕     
   
# 參考來源   
[用python实现头脑王者全自动答题](https://zhuanlan.zhihu.com/p/33290174?utm_source=com.pushbullet.android&utm_medium=social)   
[Github](https://github.com/251321639/wechat_brain)   
## 其他參考來源   
[Charles - 來漫談手機封包入門修改概念](http://blog.xuite.net/lucas.froums/psp/230771601-Charles+-+%E4%BE%86%E6%BC%AB%E8%AB%87%E6%89%8B%E6%A9%9F%E5%B0%81%E5%8C%85%E5%85%A5%E9%96%80%E4%BF%AE%E6%94%B9%E6%A6%82%E5%BF%B5)   
[charles抓包工具的中文乱码解决方法](http://www.cnblogs.com/xuxiaolu/p/6186410.html)   
[Charles抓包初学——解决HTTPS请求乱码](https://www.jianshu.com/p/8249151459fd)   
[Charles的HTTPS抓包方法及原理,下载安装ssl/https证书](https://zhubangbang.com/charles-https-packet-capture-method-and-principle.html)   
[Mirror Tool](https://www.charlesproxy.com/documentation/tools/mirror/)   
