# -*- coding: utf-8 -*-
"""
Created on Wed Feb 21 15:04:23 2018

@author: lorne
"""

#!/usr/bin/python
import time
import logging
import json
import configparser
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
configer = configparser.ConfigParser()
configer.read('setting.ini')


class FileEventHandler(FileSystemEventHandler):
    def __init__(self):
        FileSystemEventHandler.__init__(self)
    	
    def on_created(self,event):
        global quiz
        #logging.warning(event.src_path)
        if event.src_path == configer.get('Charles','question'):
            logging.warning(event.src_path)
            with open(configer.get('Charles','question'),'r',encoding = 'utf-8') as f:
                qa = json.load(f)
                for i in qa['data']['quiz']:
                    logging.warning('Q:'+i['q'])
                    logging.warning('A:'+i['o'][0])

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    
    event_handler = FileEventHandler()
    observer = Observer()
    observer.schedule(event_handler, path=configer.get('Charles','folder'), recursive=True)
    observer.start()

    try:
        time.sleep(10)
        #while True:
        #    time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    except: pass
    observer.join()